package tech.zdev.payment.bbva.util;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.util.StringUtils;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;

public class JsonUtil {

    private String datePattern;

    public JsonUtil() {
    }

    public JsonUtil(String datePattern) {
        this.datePattern = datePattern;
    }

    public <T> T parseFromJSONFile(Class<T> tClass, String filePath) {
        ClassLoader classLoader = getClass().getClassLoader();
        File jsonFile = new File(classLoader.getResource(filePath).getFile());
        ObjectMapper objectMapper = new ObjectMapper()
                .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        if (!StringUtils.isEmpty(datePattern)) {
            objectMapper.setDateFormat(new SimpleDateFormat(datePattern));
        }
        T object = null;
        try {
            object = objectMapper.readValue(jsonFile, tClass);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return object;
    }

    public <T> String parseAsString(Class<T> tClass, String file) {
        T object = parseFromJSONFile(tClass, file);
        try {
            return new ObjectMapper().writeValueAsString(object);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String objectToJson(Object object) {
        ObjectMapper mapper = new ObjectMapper();
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        mapper.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
        String jsonInString = null;
        try {
            jsonInString = mapper.writeValueAsString(object);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return jsonInString;
    }
}
