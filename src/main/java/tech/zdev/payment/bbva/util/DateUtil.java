package tech.zdev.payment.bbva.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtil {

    public DateUtil() {
    }

    public static Date parseDate(String dateInput, String inputFormat) {
        Date date = null;
        try {
            date = new SimpleDateFormat(inputFormat).parse(dateInput);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    public static String formatDate(String dateInput, String inputFormat, String outputFormat) {
        String dateOutput = null;
        try {
            Date date = new SimpleDateFormat(inputFormat).parse(dateInput);
            dateOutput = new SimpleDateFormat(outputFormat).format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return dateOutput;
    }

    public static String formatDate(Date date, String outputFormat) {
        return new SimpleDateFormat(outputFormat).format(date);
    }

}
