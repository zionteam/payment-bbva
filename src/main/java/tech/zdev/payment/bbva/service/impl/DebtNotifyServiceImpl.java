package tech.zdev.payment.bbva.service.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import tech.zdev.khipu.commons.receipt.bean.PaymentTransaction;
import tech.zdev.khipu.commons.receipt.bean.ReceiptTransactionInsertRS;
import tech.zdev.payment.bank.commons.util.DateUtil;
import tech.zdev.payment.bbva.bean.*;
import tech.zdev.payment.bbva.bean.api.BankPaymentNotification;
import tech.zdev.payment.bbva.bean.constants.OperationCodeContracts;
import tech.zdev.payment.bbva.client.khipuservice.KhipuServiceClient;
import tech.zdev.payment.bbva.client.paymentservice.PaymentServiceClient;
import tech.zdev.payment.bbva.client.senjuservice.SenjuServiceClient;
import tech.zdev.payment.bbva.constants.BBVAConstants;
import tech.zdev.payment.bbva.service.DebtConsultService;
import tech.zdev.payment.bbva.service.DebtNotifyService;
import tech.zdev.payment.bbva.transfer.NotificarPagoTransfer;
import tech.zdev.payment.bbva.transfer.PaymentTransactionTransfer;
import tech.zdev.payment.bbva.util.JsonUtil;
import tech.zdev.payment.commons.bean.PaymentOrder;
import tech.zdev.payment.commons.bean.Provider;
import tech.zdev.payment.commons.bean.Transaction;

import java.math.BigDecimal;
import java.util.List;


@Service
public class DebtNotifyServiceImpl implements DebtNotifyService {

    private static final Logger LOGGER = LogManager.getLogger(DebtConsultService.class);

    private PaymentTransactionTransfer paymentTransactionTransfer;
    private KhipuServiceClient khipuServiceClient;
    private NotificarPagoTransfer notificarPagoTransfer;
    private PaymentServiceClient paymentServiceClient;
    private SenjuServiceClient senjuServiceClient;

    public DebtNotifyServiceImpl(PaymentTransactionTransfer paymentTransactionTransfer,
                                 KhipuServiceClient khipuServiceClient,
                                 NotificarPagoTransfer notificarPagoTransfer,
                                 PaymentServiceClient paymentServiceClient,
                                 SenjuServiceClient senjuServiceClient
                                ) {
        this.paymentTransactionTransfer = paymentTransactionTransfer;
        this.khipuServiceClient = khipuServiceClient;
        this.notificarPagoTransfer = notificarPagoTransfer;
        this.paymentServiceClient = paymentServiceClient;
        this.senjuServiceClient = senjuServiceClient;
    }

    @Override
    public NotificarPagoRS notify(NotificarPagoRQ notificarPagoRQ) {

        LOGGER.info("+----------------------- Notify process ---------------------+");
        LOGGER.info("NotificarPagoRQ {}", notificarPagoRQ.toString());
        HttpStatus httpStatus = HttpStatus.NO_CONTENT;

        Operacion operacion = notificarPagoRQ.getNotificarPago()
                .getRecaudosRq()
                .getCabecera()
                .getOperacion();

        String operationNumber = operacion.getNumeroOperacion();
        String agreement = operacion.getCodigoConvenio();

        LOGGER.info("agreement {} operationNumber {} Operation Code {}", agreement, operationNumber,
                operacion.getCodigoOperacion());

        TransaccionRequest transaccion = notificarPagoRQ.getNotificarPago()
                .getRecaudosRq()
                .getDetalle()
                .getTransaccion();

        String reference = transaccion.getNumeroReferenciaDeuda().trim();
        String number = transaccion.getNumeroDocumento();
        BigDecimal amount = transaccion.getImporteDeudaPagada();

        LOGGER.info("reference {} amount {}", reference, amount);
        LOGGER.info("number {}", number);
        boolean inside = true;
        if (agreement.equalsIgnoreCase(OperationCodeContracts.RECAUDATION_BY_PAYMENT_FREEDOM)) {
            List<PaymentTransaction> paymentTransactions =
                    paymentTransactionTransfer.transfer(notificarPagoRQ, operationNumber, reference, amount);
            ReceiptTransactionInsertRS receiptTransactionInsertRS = khipuServiceClient
                    .insertPaymentTransactions(paymentTransactions);
            if (!receiptTransactionInsertRS.getProcessedReceipts().isEmpty()) {
                httpStatus = HttpStatus.OK;
            } else {
                String message = receiptTransactionInsertRS.getUnprocessedReceipts().get(0).getError().getMessage();
                if (message.equalsIgnoreCase("Duplicated PaymentTransaction")) {
                    LOGGER.error("Duplicated PaymentTransaction for number {}", operationNumber);
                    httpStatus = HttpStatus.ACCEPTED;
                }
            }
        } else {
            PaymentOrder paymentOrder = paymentServiceClient.getPaymentOrder(number);
            if (paymentOrder != null) {
                boolean isTimetable = DateUtil.checkTimetable(paymentOrder.getTimeLimit());
                LOGGER.info("is timetable? {}", isTimetable);
                inside = false;
                if (isTimetable) {
                    inside = true;
                    BankPaymentNotification bankPaymentNotification = new BankPaymentNotification();
                    bankPaymentNotification.setNumber(number);
                    Provider provider = new Provider();
                    provider.setId(BBVAConstants.PROVIDER_ID);
                    provider.setName(BBVAConstants.PROVIDER_NAME);
                    provider.setCurrency("USD");
                    provider.setCountryId("PE");
                    provider.setCode(operationNumber);
                    Transaction transaction = new Transaction();
                    transaction.setNumber(operationNumber);
                    transaction.setChannel(operacion.getCanalOperacion());
                    transaction.setDate(operacion.getFechaOperacion());
                    provider.setTransaction(transaction);
                    bankPaymentNotification.setProvider(provider);
                    LOGGER.info("bankPaymentNotificationRQ {}", JsonUtil.objectToJson(bankPaymentNotification));
                    ResponseEntity<String> responseEntity = paymentServiceClient.notifyPayment(bankPaymentNotification);
                    if (responseEntity != null) {
                        httpStatus = responseEntity.getStatusCode();
                    }
                }
            }
        }
        NotificarPagoRS notificarPagoRS = notificarPagoTransfer.transfer(notificarPagoRQ, httpStatus, inside);

        return notificarPagoRS;
    }
}
