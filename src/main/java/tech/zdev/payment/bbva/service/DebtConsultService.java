package tech.zdev.payment.bbva.service;

import tech.zdev.payment.bbva.bean.ConsultarDeudaRQ;
import tech.zdev.payment.bbva.bean.ConsultarDeudaRS;

public interface DebtConsultService {

    ConsultarDeudaRS consult(ConsultarDeudaRQ consultarDeudaRQ);


}
