package tech.zdev.payment.bbva.service;

import tech.zdev.payment.bbva.bean.ExtornarPagoRQ;
import tech.zdev.payment.bbva.bean.ExtornarPagoRS;

public interface DebtExtortService {

    ExtornarPagoRS extort(ExtornarPagoRQ extornarPagoRQ);
}
