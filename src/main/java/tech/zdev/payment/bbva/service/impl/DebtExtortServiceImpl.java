package tech.zdev.payment.bbva.service.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import tech.zdev.khipu.commons.receipt.bean.PaymentTransaction;
import tech.zdev.khipu.commons.receipt.bean.PaymentTransactionUpdateRS;
import tech.zdev.payment.bbva.bean.ExtornarPagoRQ;
import tech.zdev.payment.bbva.bean.ExtornarPagoRS;
import tech.zdev.payment.bbva.bean.Operacion;
import tech.zdev.payment.bbva.bean.TransaccionRequest;
import tech.zdev.payment.bbva.bean.constants.OperationCodeContracts;
import tech.zdev.payment.bbva.bean.emailing.MailContent;
import tech.zdev.payment.bbva.client.khipuservice.KhipuServiceClient;
import tech.zdev.payment.bbva.client.paymentservice.PaymentServiceClient;
import tech.zdev.payment.bbva.service.DebtExtortService;
import tech.zdev.payment.bbva.service.EmailingService;
import tech.zdev.payment.bbva.transfer.PaymentTransactionTransfer;
import tech.zdev.payment.commons.bean.PaymentOrder;
import tech.zdev.payment.commons.constants.PaymentOrderStatus;

import java.util.concurrent.*;

@Service
public class DebtExtortServiceImpl implements DebtExtortService {

    private static final Logger LOGGER = LogManager.getLogger(DebtExtortServiceImpl.class);
    private KhipuServiceClient khipuServiceClient;
    private PaymentTransactionTransfer paymentTransactionTransfer;
    private PaymentServiceClient paymentServiceClient;

    @Autowired
    private EmailingService emailingService;

    @Value("${mailing.to}")
    private String[] to;

    @Value("${mailing.cc}")
    private String[] cc;

    public DebtExtortServiceImpl(KhipuServiceClient khipuServiceClient,
                                 PaymentTransactionTransfer paymentTransactionTransfer,
                                 PaymentServiceClient paymentServiceClient,
                                 EmailingService emailingService) {
        this.khipuServiceClient = khipuServiceClient;
        this.paymentTransactionTransfer = paymentTransactionTransfer;
        this.paymentServiceClient = paymentServiceClient;
        this.emailingService = emailingService;
    }

    @Override
    public ExtornarPagoRS extort(ExtornarPagoRQ extornarPagoRQ) {

        LOGGER.info("+----------------------- Refund Process ---------------------+");
        LOGGER.info("ExtornarPagoRQ {}", extornarPagoRQ.toString());

        HttpStatus httpStatus = HttpStatus.NO_CONTENT;
        TransaccionRequest transaccion = extornarPagoRQ.getExtornarPago().getRecaudosRq().getDetalle().getTransaccion();
        Operacion operacion = extornarPagoRQ.getExtornarPago().getRecaudosRq().getCabecera().getOperacion();

        String numeroReferenciaDeuda = transaccion.getNumeroReferenciaDeuda().trim();
        String numeroDocumento = transaccion.getNumeroDocumento().trim();
        String cashAccount = "1021";
        String numberOperationOriginal = transaccion.getNumeroOperacionOriginal();
        String codeConvention = operacion.getCodigoConvenio();

        LOGGER.info("Code Convention {} Operation Number {} ", codeConvention, operacion.getNumeroOperacion());
        LOGGER.info("Reference Number Debt {} Document Number {}", numeroReferenciaDeuda, numeroDocumento);
        LOGGER.info("Info Payment Transaction number {}", numberOperationOriginal);
        ResponseEntity<PaymentTransaction> responseEntity1 = khipuServiceClient.getByNumber(numberOperationOriginal);
        httpStatus = responseEntity1.getStatusCode();

        String status = "CAP";
        if (httpStatus.value() == HttpStatus.OK.value()) {
            PaymentTransaction paymentTransaction = responseEntity1.getBody();
            String paymentTransactionAccountId = paymentTransaction.getAccountId();
            if (!paymentTransactionAccountId.equalsIgnoreCase(numeroDocumento)
                    && codeConvention.equalsIgnoreCase(OperationCodeContracts.RECAUDATION_BY_PAYMENT_FREEDOM)) {

                   httpStatus = HttpStatus.NO_CONTENT;

            } else if (responseEntity1.getBody().getStatus().equalsIgnoreCase(status)) {
                httpStatus = HttpStatus.NO_CONTENT;
            } else {
                LOGGER.info("PaymentTransaction Number {} to status {}", numberOperationOriginal, status);
                ResponseEntity<PaymentTransactionUpdateRS> responseEntity =
                    khipuServiceClient.updateStatusByNumberAndCashAccount(numberOperationOriginal, cashAccount, status);
                httpStatus = responseEntity.getStatusCode();

                if (codeConvention.equalsIgnoreCase(OperationCodeContracts.RECAUDATION_BY_PAYMENT_DEBT)) {
                    if (httpStatus.value() == HttpStatus.OK.value()) {
                        status = PaymentOrderStatus.PENDING_PROCESS;
                        LOGGER.info("Patch PaymentOrder number {} - status {}", numeroDocumento, status);
                        PaymentOrder paymentOrderPatch = new PaymentOrder();
                        paymentOrderPatch.setStatus(status);
                        ResponseEntity<PaymentOrder> responseEntityPatch = paymentServiceClient.patchPaymentOrder(
                                numeroDocumento,
                                paymentOrderPatch
                        );
                        httpStatus = responseEntityPatch.getStatusCode();
                    }
                }
                if (httpStatus.value() == HttpStatus.OK.value()) {
                    ExecutorService executorService = Executors.newCachedThreadPool();
                    Callable<Object> objectCallable = new Callable<Object>() {
                        @Override
                        public Object call() throws Exception {
                            MailContent mailContent = new MailContent();
                            String subject
                                    = "CostamarPayments | Interconexion BBVA | Solicitud de Anulación de Pago";
                            mailContent.setSubject(subject);
                            mailContent.setTemplate("refund-payment");
                            mailContent.setPaymentTransaction(paymentTransaction);
                            mailContent.setTo(to);
                            mailContent.setCc(cc);
                            emailingService.sendMessage(mailContent);
                            return null;
                        }
                    };
                    Future<Object> objectFuture = executorService.submit(objectCallable);
                    try {
                        objectFuture.get(1, TimeUnit.SECONDS);
                    } catch (TimeoutException timeoutException) {

                    } catch (InterruptedException interruptedException) {

                    } catch (ExecutionException executionException) {

                    } finally {
                        objectFuture.cancel(true);
                    }
                }
            }
        }
        ExtornarPagoRS extornarPagoRS = paymentTransactionTransfer.transferToExtornarPagoRS(extornarPagoRQ,
                                                                                            httpStatus,
                                                                                            numeroReferenciaDeuda);
        LOGGER.info("extornarPagoRS: {}", extornarPagoRS.toString());

        return extornarPagoRS;
    }


}
