package tech.zdev.payment.bbva.service;

import tech.zdev.payment.bbva.bean.emailing.MailContent;

public interface EmailingService {

    boolean sendSimpleMessage(String email);

    void sendMessage(MailContent mailContent);
}
