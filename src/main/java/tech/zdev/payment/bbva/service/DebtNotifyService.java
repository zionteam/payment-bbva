package tech.zdev.payment.bbva.service;

import tech.zdev.payment.bbva.bean.NotificarPagoRQ;
import tech.zdev.payment.bbva.bean.NotificarPagoRS;

public interface DebtNotifyService {
    NotificarPagoRS notify(NotificarPagoRQ notificarPagoRQ);
}
