package tech.zdev.payment.bbva.service.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import tech.zdev.payment.bank.commons.util.JsonUtil;
import tech.zdev.payment.bbva.bean.*;
import tech.zdev.payment.bbva.bean.constants.OperationCodeContracts;
import tech.zdev.payment.bbva.bean.constants.ResponseCodes;
import tech.zdev.payment.bbva.client.paymentservice.PaymentServiceClient;
import tech.zdev.payment.bbva.client.senjuservice.SenjuServiceClient;
import tech.zdev.payment.bbva.service.DebtConsultService;
import tech.zdev.payment.bbva.transfer.ConsultarDeudaTransfer;
import tech.zdev.payment.bbva.transfer.InquireTransfer;
import tech.zdev.payment.commons.bean.PaymentOrder;
import tech.zdev.payment.commons.bean.response.PaymentOrderSearchRS;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Service
public class DebtConsultServiceImpl implements DebtConsultService {

    private static final Logger LOGGER = LogManager.getLogger(DebtConsultService.class);

    private PaymentServiceClient paymentServiceClient;
    private ConsultarDeudaTransfer consultarDeudaTransfer;
    private SenjuServiceClient senjuServiceClient;
    private InquireTransfer inquireTransfer;


    @Autowired
    public DebtConsultServiceImpl(PaymentServiceClient paymentServiceClient,
                                  ConsultarDeudaTransfer consultarDeudaTransfer,
                                  SenjuServiceClient senjuServiceClient,
                                  InquireTransfer inquireTransfer
                           ) {
        this.paymentServiceClient = paymentServiceClient;
        this.consultarDeudaTransfer = consultarDeudaTransfer;
        this.senjuServiceClient = senjuServiceClient;
        this.inquireTransfer = inquireTransfer;
    }

    @Override
    public ConsultarDeudaRS consult(ConsultarDeudaRQ consultarDeudaRQ) {
        LOGGER.info("+----------------------- Consult process ---------------------+");
        LOGGER.info("ConsultarDeudaRQ {}", JsonUtil.objectToJson(consultarDeudaRQ));
        Operacion operacion = consultarDeudaRQ.getConsultarDeuda()
                .getRecaudosRq()
                .getCabecera()
                .getOperacion();

        String codeContract = operacion.getCodigoConvenio();
        String numeroOperacion = operacion.getNumeroOperacion();
        LOGGER.info("Code Convention {} Operation Number {} ", codeContract, numeroOperacion);


        TransaccionRequest transaccion = consultarDeudaRQ.getConsultarDeuda()
                .getRecaudosRq()
                .getDetalle()
                .getTransaccion();

        String reference = transaccion.getNumeroReferenciaDeuda().trim();
        LOGGER.info("Reference Number Debt {}", reference);
        Cabecera cabecera = inquireTransfer.transfer(consultarDeudaRQ);
        String code = ResponseCodes.TRANSACTION_UNREALIZED_CODE;
        String description = ResponseCodes.TRANSACTION_UNREALIZED_DESCRIPTION;
        RecaudosRS recaudosRS =  inquireTransfer.transfer(cabecera, code, description);
        ConsultarDeudaRS consultarDeudaRS = inquireTransfer.transfer(recaudosRS);

        try {
            if (codeContract.equalsIgnoreCase(OperationCodeContracts.RECAUDATION_BY_PAYMENT_FREEDOM)) {
                ResponseEntity<AccountId> responseEntity = senjuServiceClient.existAccount(reference);
                String clientName = "";
                if (responseEntity != null) {
                    if (responseEntity.getStatusCode().value() == HttpStatus.OK.value()) {
                        AccountId account = responseEntity.getBody();
                        if (!account.getAccountId().equals("NOACCT")) {
                            clientName = account.getName();
                        }
                    }
                    consultarDeudaRS = consultarDeudaTransfer.transferAgreementFree(consultarDeudaRQ, clientName);
                }
            } else {
                Map<String, String[]> params = new HashMap<>();
                params.put("status", new String[]{"PP"});
                params.put("paymentMethodCodes", new String[]{"IBBVA,IBCP"});
                params.put("limit", new String[]{"8"});
                params.put("accountId", new String[]{reference});

                PaymentOrderSearchRS paymentOrderSearchRS = paymentServiceClient.getPaymentOrdersBy(params);
                LOGGER.info("paymentOrderSearchRS {}", JsonUtil.objectToJson(paymentOrderSearchRS));
                List<PaymentOrder> paymentOrders = paymentOrderSearchRS.getPaymentOrders();
                consultarDeudaRS = consultarDeudaTransfer.transferAgreementDebt(consultarDeudaRQ, paymentOrders,
                        reference
                );
            }
        } catch (Exception e) {
            LOGGER.error("exception {} {}", e.getMessage(), e.getStackTrace());

        }
        LOGGER.info("consultarDeudaRS {}", JsonUtil.objectToJson(consultarDeudaRS));
        return consultarDeudaRS;
    }
}
