package tech.zdev.payment.bbva.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;
import org.thymeleaf.TemplateEngine;
import tech.zdev.khipu.commons.receipt.bean.PaymentTransaction;
import tech.zdev.payment.bbva.bean.emailing.MailContent;
import tech.zdev.payment.bbva.service.EmailingService;

import javax.mail.internet.MimeMessage;
import java.util.Locale;

@Service
public class EmailingServiceImpl implements EmailingService {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    private JavaMailSender javaMailSender;
    private TemplateEngine templateEngine;

    @Autowired
    public EmailingServiceImpl(JavaMailSender javaMailSender, TemplateEngine templateEngine) {
        this.javaMailSender = javaMailSender;
        this.templateEngine = templateEngine;
    }

    @Override
    public boolean sendSimpleMessage(String email) {
        boolean result = true;
        try {
            MimeMessage mimeMessage = javaMailSender.createMimeMessage();
            MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage, true, "UTF-8");
            mimeMessageHelper.setSubject("Payment BBVA | Simple Mail Test");
            mimeMessageHelper.setFrom("non-reply3@costamar.com", "payment BBVA");
            mimeMessageHelper.setTo(email);
            String text = "This is message test from payment bbva connector";
            mimeMessageHelper.setText(text, true);
            logger.info(">>> sending email");
            javaMailSender.send(mimeMessage);
            logger.info("send email successfully !!! email: {}", email);

        } catch (Exception e) {
            logger.error("Error in send email: {}", e);
            result = false;
        }

        return result;
    }

    @Override
    public void sendMessage(MailContent mailContent) {
        try {
            String htmlContent = buildTemplate(mailContent.getTemplate(), mailContent.getPaymentTransaction());
            final MimeMessage mimeMessage = javaMailSender.createMimeMessage();
            final MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(
                    mimeMessage,
                    true,
                    "UTF-8"
            );
            mimeMessageHelper.setFrom(mailContent.getFrom());
            mimeMessageHelper.setTo(mailContent.getTo());
            mimeMessageHelper.setCc(mailContent.getCc());
            mimeMessageHelper.setSubject(mailContent.getSubject());
            mimeMessageHelper.setText(htmlContent, true);
            logger.info(">>> Sending message ...");
            javaMailSender.send(mimeMessage);
            logger.info("<<< Message sent successfully");

        } catch (Exception e) {
            logger.error("Error send message: {}", e);
        }
    }


    private String buildTemplate(String template, PaymentTransaction paymentTransaction) {
        final Context context = new Context(new Locale("es"));
        context.setVariable("paymentTransaction", paymentTransaction);

        return templateEngine.process(template, context);
    }


}
