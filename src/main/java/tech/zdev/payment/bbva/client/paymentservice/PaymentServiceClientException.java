package tech.zdev.payment.bbva.client.paymentservice;

public class PaymentServiceClientException extends RuntimeException {

    public PaymentServiceClientException(String message, Throwable cause) {

        super(String.format("Error on get paymentORder | parameters: %s", message), cause);

    }

    public PaymentServiceClientException(Throwable cause) {
        super(cause);
    }
}
