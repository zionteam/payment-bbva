package tech.zdev.payment.bbva.client.senjuservice;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.BufferingClientHttpRequestFactory;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import tech.zdev.payment.bbva.bean.AccountId;
import tech.zdev.payment.bbva.client.interceptor.RestLoggingInterceptor;
import tech.zdev.payment.bbva.client.paymentservice.PaymentServiceClient;

import java.util.Collections;

@Component
public class SenjuServiceClient {
    private static final Logger LOGGER = LoggerFactory.getLogger(PaymentServiceClient.class);

    @Value("${app.clients.senjuservice}")
    private String url;

    private RestTemplate restTemplate;

    public SenjuServiceClient() {
        ClientHttpRequestFactory factory =
                new BufferingClientHttpRequestFactory(new SimpleClientHttpRequestFactory());
        this.restTemplate = new RestTemplate(factory);
        this.restTemplate.setInterceptors(Collections.singletonList(new RestLoggingInterceptor()));

    }

    public ResponseEntity<AccountId> existAccount(String accountId) {
        ResponseEntity<AccountId> responseEntity = null;

        String uri = url + "/account/" + accountId;
        try {
            responseEntity = restTemplate.exchange(uri, HttpMethod.GET, null, AccountId.class);
        } catch (Exception e) {
            LOGGER.error("message {}, trace {}", e.getMessage(), e.getStackTrace());
        }
        return responseEntity;
    }
}
