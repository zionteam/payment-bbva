package tech.zdev.payment.bbva.client.paymentservice;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import tech.zdev.payment.bank.commons.util.JsonUtil;
import tech.zdev.payment.bbva.bean.api.BankPaymentNotification;
import tech.zdev.payment.commons.bean.PaymentOrder;
import tech.zdev.payment.commons.bean.response.PaymentOrderSearchRS;

import java.util.Map;

@Component
public class PaymentServiceClient {

    private static final Logger LOGGER = LoggerFactory.getLogger(PaymentServiceClient.class);

    @Value("${app.clients.paymentservice}")
    private String url;

    private RestTemplate restTemplate;

    public PaymentServiceClient() {
        this.restTemplate = new RestTemplate();
        HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();
        this.restTemplate.setRequestFactory(requestFactory);
    }


    public PaymentOrderSearchRS getPaymentOrdersBy(Map<String, String[]> params) {
        PaymentOrderSearchRS paymentOrderSearchRS = null;
        try {
            String fullUrl = buildInvoicesUrl(params);
            LOGGER.info(">>> Request uri {}", fullUrl);
            ResponseEntity<PaymentOrderSearchRS> responseEntity = restTemplate.exchange(
                    fullUrl,
                    HttpMethod.GET,
                    null,
                    PaymentOrderSearchRS.class);
            paymentOrderSearchRS = responseEntity.getBody();
            LOGGER.info("<<< Response", JsonUtil.objectToJson(paymentOrderSearchRS));
        } catch (Exception e) {
            throw new PaymentServiceClientException(e);
        }
        return paymentOrderSearchRS;
    }


    public String buildInvoicesUrl(Map<String, String[]> params) {
        UriComponentsBuilder builder = UriComponentsBuilder
                .fromUriString(url + "/paymentorders/searchByParams");
        for (Map.Entry<String, String[]> entry : params.entrySet()) {
            String paramValues;
            if (entry.getValue().length > 1) {
                paramValues = String.join(",", entry.getValue());
            } else {
                paramValues = entry.getValue()[0];
            }
            builder.queryParam(entry.getKey(), paramValues);
        }
        return builder.toUriString();
    }

    public PaymentOrder getPaymentOrder(String number) {
        PaymentOrder paymentOrder = null;
        try {
            String resource = "/number/" + number;
            String fullURI = url + "/paymentorders" + resource;
            LOGGER.info(">>> Request uri {}", fullURI);
            LOGGER.info(">>> Request Method GET");
            ResponseEntity<PaymentOrder> responseEntity = restTemplate.exchange(fullURI, HttpMethod.GET,
                    null, PaymentOrder.class);
            paymentOrder = responseEntity.getBody();
            LOGGER.info("<<< Response body {}", JsonUtil.objectToJson(paymentOrder));
        } catch (Exception e) {
            LOGGER.error("exception occurred {}", e.getMessage());
        }
        return paymentOrder;
    }

    public ResponseEntity<PaymentOrder> patchPaymentOrder(String number, PaymentOrder paymentOrder) {
        ResponseEntity<PaymentOrder> responseEntity = null;
        try {
            String uri = url + "/paymentorders/number/" + number;
            LOGGER.info(">>> Request uri {}", uri);
            LOGGER.info(">>> Request body {}", paymentOrder.toString());
            responseEntity = restTemplate.exchange(uri, HttpMethod.PATCH, new HttpEntity<>(paymentOrder),
                    PaymentOrder.class);
            LOGGER.info("<<< Response body {}", new ObjectMapper().writeValueAsString(responseEntity.getBody()));
        } catch (Exception exception) {
            LOGGER.error(exception.getMessage());
        }

        return  responseEntity;
    }

    public ResponseEntity<String> notifyPayment(BankPaymentNotification bankPaymentNotification) {
        ResponseEntity<String> responseEntity = null;
        try {
            String fullURI = url + "/notify/bank-interconnection";
            LOGGER.info(">>> Request uri {}", fullURI);
            LOGGER.info(">>> Request Method POST");
            LOGGER.info(">>> Request body {}", JsonUtil.objectToJson(bankPaymentNotification));
            responseEntity = restTemplate.exchange(fullURI, HttpMethod.POST, new HttpEntity<>(bankPaymentNotification),
                    String.class);
        } catch (Exception e) {
            LOGGER.error("message {}, trace {}", e.getMessage(), e.getStackTrace());
        }

        return responseEntity;
    }

}
