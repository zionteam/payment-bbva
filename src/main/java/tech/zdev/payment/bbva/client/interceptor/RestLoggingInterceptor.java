package tech.zdev.payment.bbva.client.interceptor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.util.StreamUtils;

import java.io.IOException;
import java.nio.charset.Charset;

public class RestLoggingInterceptor implements ClientHttpRequestInterceptor {

    private static final Logger LOGGER = LoggerFactory.getLogger(RestLoggingInterceptor.class);

    @Override
    public ClientHttpResponse intercept(HttpRequest httpRequest, byte[] bytes,
                                        ClientHttpRequestExecution clientHttpRequestExecution)
            throws IOException {
        logRequest(httpRequest, bytes);
        ClientHttpResponse clientHttpResponse = clientHttpRequestExecution.execute(httpRequest, bytes);
        logResponse(clientHttpResponse);
        return clientHttpResponse;
    }


    private void logRequest(HttpRequest request, byte[] body) throws IOException {
        if (LOGGER.isInfoEnabled()) {
            LOGGER.info("==========================request begin======================================>>");
            LOGGER.info("URI         : {}", request.getURI());
            LOGGER.info("Method      : {}", request.getMethod());
            LOGGER.info("Headers     : {}", request.getHeaders());
            LOGGER.info("Request body: {}", new String(body, "UTF-8"));
            LOGGER.info("==========================request end========================================>>");
        }
    }

    private void logResponse(ClientHttpResponse response) throws IOException {
        if (LOGGER.isInfoEnabled()) {
            LOGGER.info("<<========================response begin=======================================");
            LOGGER.info("Status code  : {}", response.getStatusCode());
            LOGGER.info("Status text  : {}", response.getStatusText());
            LOGGER.info("Headers      : {}", response.getHeaders());
            LOGGER.info("Response body: {}", StreamUtils.copyToString(response.getBody(), Charset.defaultCharset()));
            LOGGER.info("<<========================response end=========================================");
        }
    }
}
