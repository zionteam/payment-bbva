package tech.zdev.payment.bbva.client.khipuservice;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.BufferingClientHttpRequestFactory;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import tech.zdev.khipu.commons.receipt.bean.PaymentTransaction;
import tech.zdev.khipu.commons.receipt.bean.PaymentTransactionUpdateRS;
import tech.zdev.khipu.commons.receipt.bean.ReceiptTransactionInsertRS;
import tech.zdev.payment.bbva.client.interceptor.RestLoggingInterceptor;
import tech.zdev.payment.bbva.client.paymentservice.PaymentServiceClient;

import java.util.Collections;
import java.util.List;

@Component
public class KhipuServiceClient {

    private static final Logger LOGGER = LoggerFactory.getLogger(PaymentServiceClient.class);

    @Value("${app.clients.khipuservice}")
    private String url;

    private RestTemplate restTemplate;

    public KhipuServiceClient() {
        ClientHttpRequestFactory factory =
                new BufferingClientHttpRequestFactory(new SimpleClientHttpRequestFactory());
        this.restTemplate = new RestTemplate(factory);
        this.restTemplate.setInterceptors(Collections.singletonList(new RestLoggingInterceptor()));
    }

    public ReceiptTransactionInsertRS insertPaymentTransactions(List<PaymentTransaction> paymentTransactions) {

        ReceiptTransactionInsertRS receiptTransactionInsertRS = new ReceiptTransactionInsertRS();

        try {
            HttpEntity<List<PaymentTransaction>> request = new HttpEntity<>(paymentTransactions);
            String fullUrl = url + "paymentTransactions";
            receiptTransactionInsertRS = restTemplate.postForObject(fullUrl, request, ReceiptTransactionInsertRS.class);
        } catch (Exception e) {
            LOGGER.error("error on insert paymentsTransactions", e);
        }
        return receiptTransactionInsertRS;
    }


    public ResponseEntity<PaymentTransactionUpdateRS> updateStatusByNumberAndCashAccount(
                                String number, String cashAccount, String status) {

        ResponseEntity<PaymentTransactionUpdateRS> responseEntity = null;

        String fullUrl = url + "paymentTransactions/updateStatusBy?number=" + number;
        fullUrl = fullUrl + "&cashAccount=" + cashAccount + "&status=" + status;

        responseEntity = restTemplate.exchange(
                fullUrl,
                HttpMethod.PUT,
                null,
                PaymentTransactionUpdateRS.class);

        return responseEntity;
    }


    public ResponseEntity<PaymentTransaction> getByNumber(String number) {
        ResponseEntity<PaymentTransaction> responseEntity;
        String uri = url + "paymentTransactions/number/" + number;
        responseEntity = restTemplate.exchange(uri, HttpMethod.GET, null, PaymentTransaction.class);
        return responseEntity;
    }

    public ResponseEntity<String> ping() {

        String uri = url + "paymentTransactions/ping";
        ResponseEntity<String> responseEntity = null;
        SimpleClientHttpRequestFactory simpleClientHttpRequestFactory = new SimpleClientHttpRequestFactory();
        //simpleClientHttpRequestFactory.setReadTimeout(90);
        ClientHttpRequestFactory clientHttpRequestFactory =
                new BufferingClientHttpRequestFactory(simpleClientHttpRequestFactory);
        restTemplate.setRequestFactory(clientHttpRequestFactory);
        try {
            responseEntity = restTemplate.exchange(uri, HttpMethod.GET, null, String.class);
        } catch (Exception e) {
            LOGGER.warn("Exception e {}", e);
        }

        return responseEntity;
    }

}
