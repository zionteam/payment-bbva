package tech.zdev.payment.bbva.bean;

public class RecaudosRS {

    private Cabecera cabecera;
    private DetalleResponse detalle;

    public DetalleResponse getDetalle() {
        return detalle;
    }

    public void setDetalle(DetalleResponse detalle) {
        this.detalle = detalle;
    }

    public Cabecera getCabecera() {
        return cabecera;
    }

    public void setCabecera(Cabecera cabecera) {
        this.cabecera = cabecera;
    }

    @Override
    public String toString() {
        return "RecaudosRS{" +
                "cabecera=" + cabecera +
                ", detalle=" + detalle +
                '}';
    }
}
