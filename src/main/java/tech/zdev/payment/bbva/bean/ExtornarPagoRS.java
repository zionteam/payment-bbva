package tech.zdev.payment.bbva.bean;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ExtornarPagoRS {

    @JsonProperty("ExtornarPagoResponse")
    private ExtornarPagoResponse extornarPagoResponse;


    public ExtornarPagoResponse getExtornarPagoResponse() {
        return extornarPagoResponse;
    }

    public void setExtornarPagoResponse(ExtornarPagoResponse extornarPagoResponse) {
        this.extornarPagoResponse = extornarPagoResponse;
    }

    @Override
    public String toString() {
        return "ExtornarPagoRS{" +
                "extornarPagoResponse=" + extornarPagoResponse +
                '}';
    }
}
