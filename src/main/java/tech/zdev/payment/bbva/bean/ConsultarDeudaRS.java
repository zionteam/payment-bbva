package tech.zdev.payment.bbva.bean;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ConsultarDeudaRS {

    @JsonProperty("ConsultarDeudaResponse")
    private ConsultarDeudaResponse consultarDeudaResponse;


    public ConsultarDeudaResponse getConsultarDeudaResponse() {
        return consultarDeudaResponse;
    }

    public void setConsultarDeudaResponse(ConsultarDeudaResponse consultarDeudaResponse) {
        this.consultarDeudaResponse = consultarDeudaResponse;
    }
}
