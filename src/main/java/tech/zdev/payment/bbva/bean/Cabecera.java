package tech.zdev.payment.bbva.bean;

public class Cabecera {

    private Operacion operacion;

    public Operacion getOperacion() {
        return operacion;
    }

    public void setOperacion(Operacion operacion) {
        this.operacion = operacion;
    }

    @Override
    public String toString() {
        return "Cabecera{" +
                "operacion=" + operacion +
                '}';
    }
}
