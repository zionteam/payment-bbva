package tech.zdev.payment.bbva.bean;

import com.fasterxml.jackson.annotation.JsonProperty;

public class NotificarPagoRS {

    @JsonProperty("NotificarPagoResponse")
    private NotificarPagoResponse notificarPagoResponse;

    public NotificarPagoResponse getNotificarPagoResponse() {
        return notificarPagoResponse;
    }

    public void setNotificarPagoResponse(NotificarPagoResponse notificarPagoResponse) {
        this.notificarPagoResponse = notificarPagoResponse;
    }
}
