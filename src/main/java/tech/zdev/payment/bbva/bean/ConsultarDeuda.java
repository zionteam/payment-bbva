package tech.zdev.payment.bbva.bean;

public class ConsultarDeuda {

    private RecaudosRQ recaudosRq;

    public RecaudosRQ getRecaudosRq() {
        return recaudosRq;
    }

    public void setRecaudosRq(RecaudosRQ recaudosRq) {
        this.recaudosRq = recaudosRq;
    }

    @Override
    public String toString() {
        return "ConsultarDeuda{" +
                "recaudosRq=" + recaudosRq +
                '}';
    }
}
