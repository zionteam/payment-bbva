package tech.zdev.payment.bbva.bean.constants;

public class ResponseCodes {

    public static final String SUCCESSFUL_TRANSACTION_CODE = "0001";
    public static final String SUCCESSFUL_TRANSACTION_DESCRIPTION = "TRANSACCION REALIZADA CON EXITO";
    public static final String INVALID_FRAME_FORMAT_CODE = "3000";
    public static final String INVALID_FRAME_FORMAT_DESCRIPTION = "FORMATO DE TRAMA NO VALIDO";
    public static final String TRANSACTION_UNREALIZED_CODE = "3002";
    public static final String TRANSACTION_UNREALIZED_DESCRIPTION = "NO SE PUDO REALIZAR LA TRANSACCION";
    public static final String EXTERNAL_NOT_REALIZE_CODE = "3004";
    public static final String EXTERNAL_NOT_REALIZE_DESCRIPTION = "NO SE PUEDE REALIZAR EL REGISTRO DE EXTORNO";
    public static final String NO_DEBTS_CODE = "3009";
    public static final String NO_DEBTS_DESCRIPTION = "NO TIENE DEUDAS PENDIENTES";
    public static final String REFERENCE_NUMBER_NOT_EXIST_CODE = "0101";
    public static final String REFERENCE_NUMBER_NOT_EXIST_DESCRIPTION = "NUMERO DE REFERENCIA NO EXISTE";
    public static final String REFERENCE_NUMBER_EXPIRED_CODE = "0102";
    public static final String REFERENCE_NUMBER_EXPIRED_DESCRIPTION = "NUMERO DE REFERENCIA EXPIRADA";
    public static final String REFERENCE_NUMBER_PAID_CODE = "0106";
    public static final String REFERENCE_NUMBER_PAID_DESCRIPTION = "NUMERO DE REFERENCIA CON ESTADO PAGADO";
    public static final String MUST_PAY_THE_OLDEST_DEBT_CODE = "0290";
    public static final String MUST_PAY_THE_OLDEST_DEBT_DESCRIPTION = "ERROR DEBE PAGAR LA CUOTA MAS ANTIGUA";
    public static final String REFERENCE_NUMBER_NOT_VALID_CODE = "3013";
    public static final String REFERENCE_NUMBER_NOT_VALID_DESCRIPTION = "ESTADO DE NRO DE REFERENCIA NO VALIDO";
    public static final String EXTERNAL_UNPROCESSED_CODE = "3014";
    public static final String EXTERNAL_UNPROCESSED_DESCRIPTION =
                                                            "EXTORNO NO PROCESADO PORQUE NO EXISTE REGISTRO DEL PAGO";
    public static final String PAY_MINIMUM_MAXIMUM_CODE = "3051";
    public static final String PAY_MINIMUM_MAXIMUM_DESCRIPTION = "MONTO DE PAGO DEBE SER MINIMO O MAXIMO";

}
