package tech.zdev.payment.bbva.bean;


public class ExtornarPagoResponse {

    private RecaudosRS recaudosRs;

    public RecaudosRS getRecaudosRs() {
        return recaudosRs;
    }

    public void setRecaudosRs(RecaudosRS recaudosRs) {
        this.recaudosRs = recaudosRs;
    }

    @Override
    public String toString() {
        return "ExtornarPagoResponse{" +
                "recaudosRs=" + recaudosRs +
                '}';
    }
}
