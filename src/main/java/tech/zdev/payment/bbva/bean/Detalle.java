package tech.zdev.payment.bbva.bean;

public class Detalle {

    private TransaccionRequest transaccion;

    public TransaccionRequest getTransaccion() {
        return transaccion;
    }

    public void setTransaccion(TransaccionRequest transaccion) {
        this.transaccion = transaccion;
    }

    @Override
    public String toString() {
        return "Detalle{" +
                "transaccion=" + transaccion +
                '}';
    }
}
