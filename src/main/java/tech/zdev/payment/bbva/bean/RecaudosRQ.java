package tech.zdev.payment.bbva.bean;

public class RecaudosRQ {

    private Cabecera cabecera;
    private Detalle detalle;

    public Cabecera getCabecera() {
        return cabecera;
    }

    public void setCabecera(Cabecera cabecera) {
        this.cabecera = cabecera;
    }

    public Detalle getDetalle() {
        return detalle;
    }

    public void setDetalle(Detalle detalle) {
        this.detalle = detalle;
    }

    @Override
    public String toString() {
        return "RecaudosRQ{" +
                "cabecera=" + cabecera +
                ", detalle=" + detalle +
                '}';
    }
}
