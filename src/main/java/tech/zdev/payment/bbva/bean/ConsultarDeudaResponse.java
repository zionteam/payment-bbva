package tech.zdev.payment.bbva.bean;

public class ConsultarDeudaResponse {

    private RecaudosRS recaudosRs;

    public RecaudosRS getRecaudosRs() {
        return recaudosRs;
    }

    public void setRecaudosRs(RecaudosRS recaudosRs) {
        this.recaudosRs = recaudosRs;
    }
}
