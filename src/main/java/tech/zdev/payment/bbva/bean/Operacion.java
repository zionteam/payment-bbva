package tech.zdev.payment.bbva.bean;

public class Operacion {

    private String codigoOperacion;
    private String numeroOperacion;
    private String codigoBanco;
    private String codigoConvenio;
    private String canalOperacion;
    private String codigoOficina;
    private String fechaOperacion;
    private String horaOperacion;


    public String getCodigoOperacion() {
        return codigoOperacion;
    }

    public void setCodigoOperacion(String codigoOperacion) {
        this.codigoOperacion = codigoOperacion;
    }

    public String getNumeroOperacion() {
        return numeroOperacion;
    }

    public void setNumeroOperacion(String numeroOperacion) {
        this.numeroOperacion = numeroOperacion;
    }

    public String getCodigoBanco() {
        return codigoBanco;
    }

    public void setCodigoBanco(String codigoBanco) {
        this.codigoBanco = codigoBanco;
    }

    public String getCodigoConvenio() {
        return codigoConvenio;
    }

    public void setCodigoConvenio(String codigoConvenio) {
        this.codigoConvenio = codigoConvenio;
    }

    public String getCanalOperacion() {
        return canalOperacion;
    }

    public void setCanalOperacion(String canalOperacion) {
        this.canalOperacion = canalOperacion;
    }

    public String getCodigoOficina() {
        return codigoOficina;
    }

    public void setCodigoOficina(String codigoOficina) {
        this.codigoOficina = codigoOficina;
    }

    public String getFechaOperacion() {
        return fechaOperacion;
    }

    public void setFechaOperacion(String fechaOperacion) {
        this.fechaOperacion = fechaOperacion;
    }

    public String getHoraOperacion() {
        return horaOperacion;
    }

    public void setHoraOperacion(String horaOperacion) {
        this.horaOperacion = horaOperacion;
    }

    @Override
    public String toString() {
        return "Operacion{" +
                "codigoOperacion='" + codigoOperacion + '\'' +
                ", numeroOperacion='" + numeroOperacion + '\'' +
                ", codigoBanco='" + codigoBanco + '\'' +
                ", codigoConvenio='" + codigoConvenio + '\'' +
                ", canalOperacion='" + canalOperacion + '\'' +
                ", codigoOficina='" + codigoOficina + '\'' +
                ", fechaOperacion='" + fechaOperacion + '\'' +
                ", horaOperacion='" + horaOperacion + '\'' +
                '}';
    }
}
