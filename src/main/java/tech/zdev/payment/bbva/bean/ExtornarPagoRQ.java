package tech.zdev.payment.bbva.bean;

public class ExtornarPagoRQ {

    public ExtornarPago ExtornarPago;

    public ExtornarPago getExtornarPago() {
        return ExtornarPago;
    }

    public void setExtornarPago(ExtornarPago extornarPago) {
        ExtornarPago = extornarPago;
    }

    @Override
    public String toString() {
        return "ExtornarPagoRQ{" +
                "ExtornarPago=" + ExtornarPago +
                '}';
    }
}
