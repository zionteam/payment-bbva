package tech.zdev.payment.bbva.bean;

public class ConsultarDeudaRQ {

    public ConsultarDeuda ConsultarDeuda;

    public ConsultarDeuda getConsultarDeuda() {
        return ConsultarDeuda;
    }

    public void setConsultarDeuda(ConsultarDeuda consultarDeuda) {
        ConsultarDeuda = consultarDeuda;
    }

    @Override
    public String toString() {
        return "ConsultarDeudaRQ{" +
                "ConsultarDeuda=" + ConsultarDeuda +
                '}';
    }
}
