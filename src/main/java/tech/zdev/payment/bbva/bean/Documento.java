package tech.zdev.payment.bbva.bean;

public class Documento {

    private String numero;
    private String descripcion;
    private String fechaEmision;
    private String fechaVencimiento;
    private String importeDeuda;
    private String importeDeudaMinima;
    private String indicadorRestriccPago;
    private String cantidadSubconceptos;

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getFechaEmision() {
        return fechaEmision;
    }

    public void setFechaEmision(String fechaEmision) {
        this.fechaEmision = fechaEmision;
    }

    public String getFechaVencimiento() {
        return fechaVencimiento;
    }

    public void setFechaVencimiento(String fechaVencimiento) {
        this.fechaVencimiento = fechaVencimiento;
    }

    public String getImporteDeuda() {
        return importeDeuda;
    }

    public void setImporteDeuda(String importeDeuda) {
        this.importeDeuda = importeDeuda;
    }

    public String getImporteDeudaMinima() {
        return importeDeudaMinima;
    }

    public void setImporteDeudaMinima(String importeDeudaMinima) {
        this.importeDeudaMinima = importeDeudaMinima;
    }

    public String getIndicadorRestriccPago() {
        return indicadorRestriccPago;
    }

    public void setIndicadorRestriccPago(String indicadorRestriccPago) {
        this.indicadorRestriccPago = indicadorRestriccPago;
    }

    public String getCantidadSubconceptos() {
        return cantidadSubconceptos;
    }

    public void setCantidadSubconceptos(String cantidadSubconceptos) {
        this.cantidadSubconceptos = cantidadSubconceptos;
    }
}
