package tech.zdev.payment.bbva.bean.api;

import tech.zdev.payment.commons.bean.Provider;

public class BankPaymentNotification {
    private String number;
    private Provider provider;

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Provider getProvider() {
        return provider;
    }

    public void setProvider(Provider provider) {
        this.provider = provider;
    }
}
