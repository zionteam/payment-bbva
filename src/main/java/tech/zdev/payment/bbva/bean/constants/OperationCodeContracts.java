package tech.zdev.payment.bbva.bean.constants;

public class OperationCodeContracts {
    public static final String RECAUDATION_BY_PAYMENT_FREEDOM = "14015";
    public static final String RECAUDATION_BY_PAYMENT_DEBT = "14016";

}
