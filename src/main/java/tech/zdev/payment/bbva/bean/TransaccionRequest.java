package tech.zdev.payment.bbva.bean;

import java.math.BigDecimal;

public class TransaccionRequest {
    private String numeroReferenciaDeuda;

    private String numeroDocumento;
    private BigDecimal importeDeudaPagada;
    private String numeroOperacionRecaudos;
    private String formaPago;
    private String codigoMoneda;

    private String numeroOperacionOriginal;
    private String fechaOperacionOriginal;


    public String getNumeroReferenciaDeuda() {
        return numeroReferenciaDeuda;
    }

    public void setNumeroReferenciaDeuda(String numeroReferenciaDeuda) {
        this.numeroReferenciaDeuda = numeroReferenciaDeuda;
    }

    public String getNumeroDocumento() {
        return numeroDocumento;
    }

    public void setNumeroDocumento(String numeroDocumento) {
        this.numeroDocumento = numeroDocumento;
    }

    public BigDecimal getImporteDeudaPagada() {
        return importeDeudaPagada;
    }

    public void setImporteDeudaPagada(BigDecimal importeDeudaPagada) {
        this.importeDeudaPagada = importeDeudaPagada;
    }

    public String getNumeroOperacionRecaudos() {
        return numeroOperacionRecaudos;
    }

    public void setNumeroOperacionRecaudos(String numeroOperacionRecaudos) {
        this.numeroOperacionRecaudos = numeroOperacionRecaudos;
    }

    public String getFormaPago() {
        return formaPago;
    }

    public void setFormaPago(String formaPago) {
        this.formaPago = formaPago;
    }

    public String getCodigoMoneda() {
        return codigoMoneda;
    }

    public void setCodigoMoneda(String codigoMoneda) {
        this.codigoMoneda = codigoMoneda;
    }

    public String getNumeroOperacionOriginal() {
        return numeroOperacionOriginal;
    }

    public void setNumeroOperacionOriginal(String numeroOperacionOriginal) {
        this.numeroOperacionOriginal = numeroOperacionOriginal;
    }

    public String getFechaOperacionOriginal() {
        return fechaOperacionOriginal;
    }

    public void setFechaOperacionOriginal(String fechaOperacionOriginal) {
        this.fechaOperacionOriginal = fechaOperacionOriginal;
    }

    @Override
    public String toString() {
        return "TransaccionRequest{" +
                "numeroReferenciaDeuda='" + numeroReferenciaDeuda + '\'' +
                ", numeroDocumento='" + numeroDocumento + '\'' +
                ", importeDeudaPagada=" + importeDeudaPagada +
                ", numeroOperacionRecaudos='" + numeroOperacionRecaudos + '\'' +
                ", formaPago='" + formaPago + '\'' +
                ", codigoMoneda='" + codigoMoneda + '\'' +
                ", numeroOperacionOriginal='" + numeroOperacionOriginal + '\'' +
                ", fechaOperacionOriginal='" + fechaOperacionOriginal + '\'' +
                '}';
    }
}
