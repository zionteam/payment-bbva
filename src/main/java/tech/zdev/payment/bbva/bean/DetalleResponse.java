package tech.zdev.payment.bbva.bean;

public class DetalleResponse {

    private Respuesta respuesta;
    private TransaccionResponse transaccion;


    public TransaccionResponse getTransaccion() {
        return transaccion;
    }

    public void setTransaccion(TransaccionResponse transaccion) {
        this.transaccion = transaccion;
    }


    public Respuesta getRespuesta() {
        return respuesta;
    }

    public void setRespuesta(Respuesta respuesta) {
        this.respuesta = respuesta;
    }

    @Override
    public String toString() {
        return "DetalleResponse{" +
                "respuesta=" + respuesta +
                ", transaccion=" + transaccion +
                '}';
    }
}
