package tech.zdev.payment.bbva.bean;



public class NotificarPagoResponse {

    private RecaudosRS recaudosRs;

    public RecaudosRS getRecaudosRs() {
        return recaudosRs;
    }

    public void setRecaudosRs(RecaudosRS recaudosRs) {
        this.recaudosRs = recaudosRs;
    }
}
