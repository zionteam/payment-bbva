package tech.zdev.payment.bbva.bean;

public class NotificarPagoRQ {
    public NotificarPago NotificarPago;


    public NotificarPago getNotificarPago() {
        return NotificarPago;
    }

    public void setNotificarPago(NotificarPago notificarPago) {
        NotificarPago = notificarPago;
    }

    @Override
    public String toString() {
        return "NotificarPagoRQ{" +
                "NotificarPago=" + NotificarPago +
                '}';
    }
}
