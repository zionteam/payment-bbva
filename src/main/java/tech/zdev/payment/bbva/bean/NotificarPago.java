package tech.zdev.payment.bbva.bean;

public class NotificarPago {

    private RecaudosRQ recaudosRq;

    public RecaudosRQ getRecaudosRq() {
        return recaudosRq;
    }

    public void setRecaudosRq(RecaudosRQ recaudosRq) {
        this.recaudosRq = recaudosRq;
    }

    @Override
    public String toString() {
        return "NotificarPago{" +
                "recaudosRq=" + recaudosRq +
                '}';
    }
}
