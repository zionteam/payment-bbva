package tech.zdev.payment.bbva.bean.constants;

public class TimeLimit {

    public static final int HOUR = 19;
    public static final int MINUTE = 30;

}
