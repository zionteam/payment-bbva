package tech.zdev.payment.bbva.bean;


import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class TransaccionResponse {

    private String numeroReferenciaDeuda;


    private String nombreCliente;

    private String numeroOperacionEmpresa;

    private Integer indMasDeuda;
    private Integer cantidadDocsDeuda;
    private String datosEmpresa;
    private ListaDocumentos listaDocumentos;

    public String getNumeroReferenciaDeuda() {
        return numeroReferenciaDeuda;
    }

    public void setNumeroReferenciaDeuda(String numeroReferenciaDeuda) {
        this.numeroReferenciaDeuda = numeroReferenciaDeuda;
    }

    public String getNombreCliente() {
        return nombreCliente;
    }

    public void setNombreCliente(String nombreCliente) {
        this.nombreCliente = nombreCliente;
    }

    public String getNumeroOperacionEmpresa() {
        return numeroOperacionEmpresa;
    }

    public void setNumeroOperacionEmpresa(String numeroOperacionEmpresa) {
        this.numeroOperacionEmpresa = numeroOperacionEmpresa;
    }

    public Integer getCantidadDocsDeuda() {
        return cantidadDocsDeuda;
    }

    public void setCantidadDocsDeuda(Integer cantidadDocsDeuda) {
        this.cantidadDocsDeuda = cantidadDocsDeuda;
    }

    public String getDatosEmpresa() {
        return datosEmpresa;
    }

    public void setDatosEmpresa(String datosEmpresa) {
        this.datosEmpresa = datosEmpresa;
    }

    public ListaDocumentos getListaDocumentos() {
        return listaDocumentos;
    }

    public void setListaDocumentos(ListaDocumentos listaDocumentos) {
        this.listaDocumentos = listaDocumentos;
    }

    public Integer getIndMasDeuda() {
        return indMasDeuda;
    }

    public void setIndMasDeuda(Integer indMasDeuda) {
        this.indMasDeuda = indMasDeuda;
    }

    @Override
    public String toString() {
        return "TransaccionResponse{" +
                "numeroReferenciaDeuda='" + numeroReferenciaDeuda + '\'' +
                ", nombreCliente='" + nombreCliente + '\'' +
                ", numeroOperacionEmpresa='" + numeroOperacionEmpresa + '\'' +
                ", indMasDeuda=" + indMasDeuda +
                ", cantidadDocsDeuda=" + cantidadDocsDeuda +
                ", datosEmpresa='" + datosEmpresa + '\'' +
                ", listaDocumentos=" + listaDocumentos +
                '}';
    }
}
