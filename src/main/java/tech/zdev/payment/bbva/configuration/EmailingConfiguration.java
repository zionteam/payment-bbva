package tech.zdev.payment.bbva.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.mail.javamail.JavaMailSenderImpl;

@Configuration
@PropertySource("classpath:mailing.properties")
public class EmailingConfiguration {

    @Value("${mailing.host}")
    private String host;
    @Value("${mailing.port}")
    private int port;
    @Value("${mailing.protocol}")
    private String protocol;
    @Value("${mailing.username}")
    private String username;
    @Value("${mailing.password}")
    private String password;

    @Bean
    public JavaMailSenderImpl javaMailSender() {
        JavaMailSenderImpl javaMailSenderImpl = new JavaMailSenderImpl();
        javaMailSenderImpl.setHost(host);
        javaMailSenderImpl.setPort(port);
        javaMailSenderImpl.setProtocol(protocol);
        javaMailSenderImpl.setUsername(username);
        javaMailSenderImpl.setPassword(password);
        return javaMailSenderImpl;
    }

}
