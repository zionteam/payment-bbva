package tech.zdev.payment.bbva.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import tech.zdev.payment.bbva.bean.*;
import tech.zdev.payment.bbva.service.DebtConsultService;
import tech.zdev.payment.bbva.service.DebtExtortService;
import tech.zdev.payment.bbva.service.DebtNotifyService;

@RestController
public class DebtController {

    private DebtConsultService debtFindService;
    private DebtNotifyService debtProcessService;
    private DebtExtortService debtExtortService;

    @Autowired
    public DebtController(DebtConsultService debtFindService, DebtNotifyService debtProcessService,
                          DebtExtortService debtExtortService) {
        this.debtFindService = debtFindService;
        this.debtProcessService = debtProcessService;
        this.debtExtortService = debtExtortService;
    }

    @GetMapping(value = "/ping")
    public String ping() {
        return "Ping from controller Debt :D";
    }

    @PostMapping(value = "ConsultarDeuda", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<ConsultarDeudaRS> getConsult(@RequestBody ConsultarDeudaRQ consultarDeudaRQ) {

        ConsultarDeudaRS consultarDeudaRS = debtFindService.consult(consultarDeudaRQ);

        return ResponseEntity.ok(consultarDeudaRS);
    }

    @PostMapping(value = "NotificarPago", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<NotificarPagoRS> setConsult(@RequestBody NotificarPagoRQ notificarPagoRQ) {

        NotificarPagoRS notificarPagoRS = debtProcessService.notify(notificarPagoRQ);

        return ResponseEntity.ok(notificarPagoRS);

    }

    @PostMapping(value = "ExtornarPago", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<ExtornarPagoRS> setExtort(@RequestBody ExtornarPagoRQ extornarPagoRQ) {

        ExtornarPagoRS extornarPagoRS = debtExtortService.extort(extornarPagoRQ);

        return ResponseEntity.ok(extornarPagoRS);
    }



}
