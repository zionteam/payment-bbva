package tech.zdev.payment.bbva.constants;

public class BBVAConstants {
    public static final int PROVIDER_ID = 120;
    public static final String PROVIDER_NAME = "BBVA";
}
