package tech.zdev.payment.bbva;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PaymentBbvaApplication {

    public static void main(String[] args) {
        SpringApplication.run(PaymentBbvaApplication.class, args);
    }

}
