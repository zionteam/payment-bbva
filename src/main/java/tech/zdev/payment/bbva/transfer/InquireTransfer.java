package tech.zdev.payment.bbva.transfer;


import org.springframework.stereotype.Component;
import tech.zdev.payment.bbva.bean.*;

@Component
public class InquireTransfer {

    public Cabecera transfer(ConsultarDeudaRQ consultarDeudaRQ) {
        Operacion operacion = consultarDeudaRQ
                .getConsultarDeuda()
                .getRecaudosRq()
                .getCabecera()
                .getOperacion();
        Cabecera cabecera = new Cabecera();
        cabecera.setOperacion(operacion);
        return cabecera;
    }

    public RecaudosRS transfer(Cabecera cabecera, String code, String description) {
        RecaudosRS recaudosRS = new RecaudosRS();
        recaudosRS.setCabecera(cabecera);
        Respuesta respuesta = new Respuesta();
        respuesta.setCodigo(code);
        respuesta.setDescripcion(description);
        DetalleResponse detalle = new DetalleResponse();
        detalle.setRespuesta(respuesta);
        recaudosRS.setDetalle(detalle);

        return recaudosRS;
    }

    public ConsultarDeudaRS transfer(RecaudosRS recaudosRS) {
        ConsultarDeudaResponse consultarDeudaResponse = new ConsultarDeudaResponse();
        consultarDeudaResponse.setRecaudosRs(recaudosRS);

        ConsultarDeudaRS consultarDeudaRS = new ConsultarDeudaRS();
        consultarDeudaRS.setConsultarDeudaResponse(consultarDeudaResponse);

        return consultarDeudaRS;
    }
}
