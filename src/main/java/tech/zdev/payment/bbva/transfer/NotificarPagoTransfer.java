package tech.zdev.payment.bbva.transfer;

import org.springframework.http.HttpStatus;
import tech.zdev.payment.bbva.bean.NotificarPagoRQ;
import tech.zdev.payment.bbva.bean.NotificarPagoRS;

public interface NotificarPagoTransfer {


    NotificarPagoRS transfer(NotificarPagoRQ notificarPagoRQ, HttpStatus httpStatus, boolean inside);
}
