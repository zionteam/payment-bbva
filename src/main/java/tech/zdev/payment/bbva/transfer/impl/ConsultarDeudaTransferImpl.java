package tech.zdev.payment.bbva.transfer.impl;

import org.apache.logging.log4j.LogManager;
import org.springframework.stereotype.Component;
import tech.zdev.payment.bank.commons.util.DateUtil;
import tech.zdev.payment.bbva.bean.*;
import tech.zdev.payment.bbva.bean.constants.ResponseCodes;
import tech.zdev.payment.bbva.transfer.ConsultarDeudaTransfer;
import tech.zdev.payment.commons.bean.PaymentOrder;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Component("responseConsultarDeudaTransfer")
public class ConsultarDeudaTransferImpl implements ConsultarDeudaTransfer {

    private static final org.apache.logging.log4j.Logger LOGGER = LogManager.getLogger(ConsultarDeudaTransfer.class);
    private static final int MAX_DOCUMENTS = 8;

    @Override
    public ConsultarDeudaRS transfer(PaymentOrder paymentOrderRS, ConsultarDeudaRQ consultarDeudaRQ) {
        String code = ResponseCodes.REFERENCE_NUMBER_NOT_EXIST_CODE;
        String description = ResponseCodes.REFERENCE_NUMBER_NOT_EXIST_DESCRIPTION;
        Operacion operacion = consultarDeudaRQ
                            .getConsultarDeuda()
                            .getRecaudosRq()
                            .getCabecera()
                            .getOperacion();

        if (paymentOrderRS != null) {
            code = ResponseCodes.SUCCESSFUL_TRANSACTION_CODE;
            description = ResponseCodes.SUCCESSFUL_TRANSACTION_DESCRIPTION;
        }

        Respuesta respuesta = new Respuesta();
        respuesta.setCodigo(code);
        respuesta.setDescripcion(description);

        DetalleResponse detalle = new DetalleResponse();
        detalle.setRespuesta(respuesta);

        if (code.equalsIgnoreCase("0001")) {
            TransaccionResponse transaccion = new TransaccionResponse();
            transaccion.setNumeroReferenciaDeuda(paymentOrderRS.getNumber());
            transaccion.setNombreCliente(paymentOrderRS.getClient().getName());
            transaccion.setDatosEmpresa("");

            List<Documento> documentos = new ArrayList<>();

            Documento documento = new Documento();
            documento.setNumero(paymentOrderRS.getNumber());
            documento.setDescripcion("Pago Unico");
            documento.setImporteDeuda(String.valueOf(paymentOrderRS.getAmount()));
            documentos.add(documento);


            ListaDocumentos listaDocumentos = new ListaDocumentos();
            listaDocumentos.setDocumento(documentos);

            transaccion.setListaDocumentos(listaDocumentos);
            detalle.setTransaccion(transaccion);
        }
        Cabecera cabecera = new Cabecera();
        cabecera.setOperacion(operacion);
        RecaudosRS recaudosRS = new RecaudosRS();
        recaudosRS.setCabecera(cabecera);
        recaudosRS.setDetalle(detalle);
        ConsultarDeudaResponse consultarDeudaResponse = new ConsultarDeudaResponse();
        consultarDeudaResponse.setRecaudosRs(recaudosRS);
        ConsultarDeudaRS consultarDeudaRS = new ConsultarDeudaRS();
        consultarDeudaRS.setConsultarDeudaResponse(consultarDeudaResponse);

        return consultarDeudaRS;
    }

    @Override
    public ConsultarDeudaRS transferAgreementFree(ConsultarDeudaRQ consultarDeudaRQ, String clientName) {
        ConsultarDeudaRS consultarDeudaRS = new ConsultarDeudaRS();
        Operacion operacion = consultarDeudaRQ
                .getConsultarDeuda()
                .getRecaudosRq()
                .getCabecera()
                .getOperacion();

        String reference = consultarDeudaRQ.getConsultarDeuda()
                .getRecaudosRq()
                .getDetalle()
                .getTransaccion()
                .getNumeroReferenciaDeuda();

        RecaudosRS recaudosRS = new RecaudosRS();

        Cabecera cabecera = new Cabecera();
        cabecera.setOperacion(operacion);
        recaudosRS.setCabecera(cabecera);

        String code = ResponseCodes.SUCCESSFUL_TRANSACTION_CODE;
        String description = ResponseCodes.SUCCESSFUL_TRANSACTION_DESCRIPTION;
        if (clientName.isEmpty()) {
            code = ResponseCodes.REFERENCE_NUMBER_NOT_EXIST_CODE;
            description = ResponseCodes.REFERENCE_NUMBER_NOT_EXIST_DESCRIPTION;
        }

        Respuesta respuesta = new Respuesta();
        respuesta.setCodigo(code);
        respuesta.setDescripcion(description);

        DetalleResponse detalle = new DetalleResponse();
        detalle.setRespuesta(respuesta);
        TransaccionResponse transaccion = new TransaccionResponse();
        transaccion.setNumeroReferenciaDeuda(reference);
        transaccion.setNombreCliente(clientName);
        transaccion.setIndMasDeuda(0);
        transaccion.setCantidadDocsDeuda(1);
        transaccion.setDatosEmpresa("");

        List<Documento> documentos = new ArrayList<>();
        Documento documento = new Documento();
        documento.setNumero(reference);
        documento.setDescripcion("Recaudo Libre");

        LocalDateTime now = LocalDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMdd");
        String emissionDate = now.format(formatter);

        documento.setFechaEmision(emissionDate);
        documento.setFechaVencimiento(emissionDate);
        documento.setImporteDeuda("9999999.00");
        documento.setImporteDeudaMinima("1.00");
        documentos.add(documento);

        ListaDocumentos listaDocumentos = new ListaDocumentos();
        listaDocumentos.setDocumento(documentos);

        transaccion.setListaDocumentos(listaDocumentos);
        detalle.setTransaccion(transaccion);

        recaudosRS.setDetalle(detalle);
        ConsultarDeudaResponse consultarDeudaResponse = new ConsultarDeudaResponse();
        consultarDeudaResponse.setRecaudosRs(recaudosRS);
        consultarDeudaRS.setConsultarDeudaResponse(consultarDeudaResponse);

        return consultarDeudaRS;

    }

    @Override
    public ConsultarDeudaRS transferAgreementDebt(ConsultarDeudaRQ consultarDeudaRQ, List<PaymentOrder> paymentOrders,
                                        String reference) {
        String code = ResponseCodes.NO_DEBTS_CODE;
        String description = ResponseCodes.NO_DEBTS_DESCRIPTION;

        Operacion operacion = consultarDeudaRQ
                .getConsultarDeuda()
                .getRecaudosRq()
                .getCabecera()
                .getOperacion();
        Cabecera cabecera = new Cabecera();
        cabecera.setOperacion(operacion);
        RecaudosRS recaudosRS = new RecaudosRS();
        recaudosRS.setCabecera(cabecera);
        DetalleResponse detailResponse = new DetalleResponse();
        if (!paymentOrders.isEmpty()) {
            TransaccionResponse transaccion = new TransaccionResponse();
            transaccion.setNumeroReferenciaDeuda(reference);
            int paymentOrdersSize = paymentOrders.size();

            List<Documento> documents = new ArrayList<>();
            Map<String, String> map = new HashMap<>();
            for (int i = 0; i < paymentOrdersSize; i++) {
                if (i == 0) {
                    map.put("clientName", paymentOrders.get(i).getClient().getName());
                    map.put("clientEmail", paymentOrders.get(i).getClient().getEmail());
                }
                String number = paymentOrders.get(i).getNumber();
                LOGGER.info("payment {} time limit {}", number, paymentOrders.get(i).getTimeLimit());
                boolean isTimetable = DateUtil.checkTimetable(paymentOrders.get(i).getTimeLimit());
                LOGGER.info("is timetable? {}", isTimetable);
                if (isTimetable) {
                    Documento documento = new Documento();
                    documento.setNumero(number);
                    documento.setDescripcion("Pago Unico " + paymentOrders.get(i).getReference());
                    String dateEmission = DateUtil.formatDate(paymentOrders.get(i).getDatetime(),
                            "yyyy-MM-dd hh:mm:ss", "yyyyMMdd");
                    documento.setFechaEmision(dateEmission);
                    documento.setFechaVencimiento(dateEmission);
                    String debt = String.valueOf(paymentOrders.get(i).getAmount());
                    documento.setImporteDeuda(debt);
                    documento.setImporteDeudaMinima(debt);
                    documents.add(documento);
                }
            }
            if (!documents.isEmpty()) {
                code = ResponseCodes.SUCCESSFUL_TRANSACTION_CODE;
                description = ResponseCodes.SUCCESSFUL_TRANSACTION_DESCRIPTION;
                ListaDocumentos listaDocumentos = new ListaDocumentos();
                listaDocumentos.setDocumento(documents);
                if (map.size() > 0) {
                    transaccion.setNombreCliente(map.get("clientName"));
                    transaccion.setDatosEmpresa(map.get("clientEmail"));
                }
                transaccion.setListaDocumentos(listaDocumentos);
                int documentsSize = documents.size();
                int indicatorMoreDebt = 0;
                if (documentsSize > MAX_DOCUMENTS) {
                    indicatorMoreDebt = 1;
                }
                transaccion.setIndMasDeuda(indicatorMoreDebt);
                transaccion.setCantidadDocsDeuda(documentsSize);
                detailResponse.setTransaccion(transaccion);
            }
        }
        Respuesta respuesta = new Respuesta();
        respuesta.setCodigo(code);
        respuesta.setDescripcion(description);
        detailResponse.setRespuesta(respuesta);
        recaudosRS.setDetalle(detailResponse);
        ConsultarDeudaResponse consultarDeudaResponse = new ConsultarDeudaResponse();
        consultarDeudaResponse.setRecaudosRs(recaudosRS);
        ConsultarDeudaRS consultarDeudaRS = new ConsultarDeudaRS();
        consultarDeudaRS.setConsultarDeudaResponse(consultarDeudaResponse);

        return consultarDeudaRS;
    }
}
