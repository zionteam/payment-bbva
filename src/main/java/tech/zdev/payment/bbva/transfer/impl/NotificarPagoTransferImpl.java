package tech.zdev.payment.bbva.transfer.impl;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import tech.zdev.payment.bbva.bean.*;
import tech.zdev.payment.bbva.bean.constants.ResponseCodes;
import tech.zdev.payment.bbva.transfer.NotificarPagoTransfer;


@Component
public class NotificarPagoTransferImpl implements NotificarPagoTransfer {

    @Override
    public NotificarPagoRS transfer(NotificarPagoRQ notificarPagoRQ, HttpStatus httpStatus, boolean inside) {

        RecaudosRQ recaudosRQ = notificarPagoRQ.getNotificarPago().getRecaudosRq();

        Cabecera cabecera = new Cabecera();
        cabecera.setOperacion(recaudosRQ.getCabecera().getOperacion());

        TransaccionResponse transaction = new TransaccionResponse();
        transaction.setNumeroReferenciaDeuda(recaudosRQ.getDetalle().getTransaccion().getNumeroReferenciaDeuda());

        DetalleResponse detalleResponse = new DetalleResponse();

        String code = ResponseCodes.REFERENCE_NUMBER_EXPIRED_CODE;
        String description = ResponseCodes.REFERENCE_NUMBER_EXPIRED_DESCRIPTION;
        if (inside) {
            if (httpStatus.value() == HttpStatus.OK.value()) {
                code = ResponseCodes.SUCCESSFUL_TRANSACTION_CODE;
                description = ResponseCodes.SUCCESSFUL_TRANSACTION_DESCRIPTION;
            } else if (httpStatus.value() == HttpStatus.ACCEPTED.value()) {
                code = ResponseCodes.REFERENCE_NUMBER_PAID_CODE;
                description = ResponseCodes.REFERENCE_NUMBER_PAID_DESCRIPTION;
            }
        }

        Respuesta respuesta = new Respuesta();
        respuesta.setCodigo(code);
        respuesta.setDescripcion(description);

        detalleResponse.setRespuesta(respuesta);
        detalleResponse.setTransaccion(transaction);

        RecaudosRS recaudosRS = new RecaudosRS();
        recaudosRS.setCabecera(cabecera);
        recaudosRS.setDetalle(detalleResponse);

        NotificarPagoResponse notificarPagoResponse = new NotificarPagoResponse();
        notificarPagoResponse.setRecaudosRs(recaudosRS);

        NotificarPagoRS notificarPagoRS = new NotificarPagoRS();
        notificarPagoRS.setNotificarPagoResponse(notificarPagoResponse);

        return notificarPagoRS;
    }
}
