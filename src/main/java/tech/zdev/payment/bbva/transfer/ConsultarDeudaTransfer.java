package tech.zdev.payment.bbva.transfer;

import tech.zdev.payment.bbva.bean.ConsultarDeudaRQ;
import tech.zdev.payment.bbva.bean.ConsultarDeudaRS;
import tech.zdev.payment.commons.bean.PaymentOrder;

import java.util.List;


public interface ConsultarDeudaTransfer {

    ConsultarDeudaRS transfer(PaymentOrder paymentOrderRS, ConsultarDeudaRQ consultarDeudaRQ);
    ConsultarDeudaRS transferAgreementFree(ConsultarDeudaRQ consultarDeudaRQ, String clientName);
    ConsultarDeudaRS transferAgreementDebt(ConsultarDeudaRQ consultarDeudaRQ, List<PaymentOrder> paymentOrders,
                                           String reference);

}
