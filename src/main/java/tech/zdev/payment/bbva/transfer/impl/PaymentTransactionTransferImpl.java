package tech.zdev.payment.bbva.transfer.impl;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import tech.zdev.khipu.commons.receipt.bean.PaymentTransaction;
import tech.zdev.payment.bank.commons.util.DateUtil;
import tech.zdev.payment.bbva.bean.*;
import tech.zdev.payment.bbva.bean.constants.ResponseCodes;
import tech.zdev.payment.bbva.transfer.PaymentTransactionTransfer;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
public class PaymentTransactionTransferImpl implements PaymentTransactionTransfer {

    @Override
    public List<PaymentTransaction> transfer(NotificarPagoRQ notificarPagoRQ, String operationNumber, String reference,
                                                                              BigDecimal amount) {

        Operacion operacion = notificarPagoRQ.getNotificarPago().getRecaudosRq().getCabecera().getOperacion();
        Date operationDate = DateUtil.parseDate(operacion.getFechaOperacion(), "yyyyMMdd");
        List<PaymentTransaction> paymentTransactions = new ArrayList<>();
        PaymentTransaction paymentTransaction = new PaymentTransaction();
        paymentTransaction.setCashAccount("1021");
        paymentTransaction.setNumber(operationNumber);
        paymentTransaction.setCreatedBy("khipu.user");
        paymentTransaction.setCurrency("USD");
        paymentTransaction.setAccountId(reference);
        paymentTransaction.setAmount(amount);
        paymentTransaction.setType("PAYMENT FREE");
        paymentTransaction.setCustomerType("");
        paymentTransaction.setAutomaticFlow(true);
        paymentTransaction.setPaymentDate(operationDate);
        paymentTransactions.add(paymentTransaction);

        return paymentTransactions;

    }

    @Override
    public ExtornarPagoRS transferToExtornarPagoRS(ExtornarPagoRQ extornarPagoRQ,
                                                   HttpStatus httpStatus,
                                                   String reference
    ) {


        Operacion operacion = extornarPagoRQ.getExtornarPago().getRecaudosRq().getCabecera().getOperacion();

        Cabecera cabecera = new Cabecera();
        cabecera.setOperacion(operacion);

        String code = ResponseCodes.EXTERNAL_NOT_REALIZE_CODE;
        String description = ResponseCodes.EXTERNAL_NOT_REALIZE_DESCRIPTION;

        DetalleResponse detalle = new DetalleResponse();
        if (httpStatus != null && httpStatus.value() == HttpStatus.OK.value()) {
            code = ResponseCodes.SUCCESSFUL_TRANSACTION_CODE;
            description = ResponseCodes.SUCCESSFUL_TRANSACTION_DESCRIPTION;

            TransaccionResponse transaccion = new TransaccionResponse();
            transaccion.setNumeroReferenciaDeuda(reference);
            detalle.setTransaccion(transaccion);
        }

        Respuesta respuesta = new Respuesta();
        respuesta.setCodigo(code);
        respuesta.setDescripcion(description);

        detalle.setRespuesta(respuesta);


        RecaudosRS recaudoRS = new RecaudosRS();
        recaudoRS.setCabecera(cabecera);
        recaudoRS.setDetalle(detalle);

        ExtornarPagoResponse extornarPagoResponse = new ExtornarPagoResponse();
        extornarPagoResponse.setRecaudosRs(recaudoRS);

        ExtornarPagoRS extornarPagoRS = new ExtornarPagoRS();
        extornarPagoRS.setExtornarPagoResponse(extornarPagoResponse);

        return extornarPagoRS;
    }
}
