package tech.zdev.payment.bbva.transfer;

import org.springframework.http.HttpStatus;
import tech.zdev.khipu.commons.receipt.bean.PaymentTransaction;
import tech.zdev.payment.bbva.bean.ExtornarPagoRQ;
import tech.zdev.payment.bbva.bean.ExtornarPagoRS;
import tech.zdev.payment.bbva.bean.NotificarPagoRQ;

import java.math.BigDecimal;
import java.util.List;

public interface PaymentTransactionTransfer {


    List<PaymentTransaction> transfer(NotificarPagoRQ notificarPagoRQ, String operationNumber, String reference,
                                                                       BigDecimal amount);


    ExtornarPagoRS transferToExtornarPagoRS(ExtornarPagoRQ extornarPagoRQ,
                                            HttpStatus httpStatus,
                                            String numeroReferenciaDeuda);



}
