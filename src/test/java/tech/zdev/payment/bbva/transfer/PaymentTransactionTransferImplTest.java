package tech.zdev.payment.bbva.transfer;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import tech.zdev.khipu.commons.receipt.bean.PaymentTransaction;
import tech.zdev.payment.bbva.bean.AccountId;
import tech.zdev.payment.bbva.bean.NotificarPagoRQ;
import tech.zdev.payment.bbva.util.JsonUtil;


@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("dev")
public class PaymentTransactionTransferImplTest {

    @Autowired
    PaymentTransactionTransfer paymentTransactionTransfer;


    @Test
    public void test() {
        NotificarPagoRQ notificarPagoRQ = loadNotificarPagoRQ();
        paymentTransactionTransfer.transfer(notificarPagoRQ, "", "", null);
    }


    private NotificarPagoRQ loadNotificarPagoRQ() {
        JsonUtil jsonUtil = new JsonUtil();
        NotificarPagoRQ notificarPagoRQ = jsonUtil.parseFromJSONFile(NotificarPagoRQ.class,
                "NotificarPagoRQ.json");
        return notificarPagoRQ;
    }

}