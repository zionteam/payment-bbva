package tech.zdev.payment.bbva.client.paymentservice;


import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import tech.zdev.payment.commons.bean.PaymentOrder;
import tech.zdev.payment.commons.bean.PaymentProvider;
import tech.zdev.payment.commons.bean.Provider;


@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("dev")
public class PaymentServiceClientTest {

    @Autowired
    PaymentServiceClient paymentServiceClient;


    @Ignore
    @Test
    public void testPatchPaymentOrder() {
        String number = "118795340";
        PaymentOrder paymentOrder = new PaymentOrder();
        paymentOrder.setStatus("TEST");
        PaymentProvider provider = new PaymentProvider();
        provider.setId(127);
        provider.setName("BCP");
        provider.setCurrency("USD");
        provider.setCountryId("PE");
        provider.setCode("213213213");
        paymentOrder.setProvider(provider);

        ResponseEntity<PaymentOrder> responseEntity = paymentServiceClient.patchPaymentOrder(number, paymentOrder);
        responseEntity.getBody();


    }
}