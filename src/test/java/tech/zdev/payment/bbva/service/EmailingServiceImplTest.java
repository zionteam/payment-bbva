package tech.zdev.payment.bbva.service;

import junit.framework.TestCase;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("dev")
public class EmailingServiceImplTest extends TestCase {
    @Autowired
    private EmailingService emailingService;

    @Ignore
    @Test
    public void testSendSimpleMail() {
        String email = "ederrafo@gmail.com";
        assertTrue(emailingService.sendSimpleMessage(email));
    }
}