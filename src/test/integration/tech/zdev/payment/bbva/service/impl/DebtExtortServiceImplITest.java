package tech.zdev.payment.bbva.service.impl;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import tech.zdev.khipu.commons.receipt.bean.PaymentTransaction;
import tech.zdev.payment.bbva.bean.ExtornarPagoRQ;
import tech.zdev.payment.bbva.bean.ExtornarPagoRS;
import tech.zdev.payment.bbva.util.JsonUtil;


@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("dev")
public class DebtExtortServiceImplITest {

    @Autowired
    DebtExtortServiceImpl debtExtortService;

    @Test
    public void testRefund() {
        JsonUtil jsonUtil = new JsonUtil();
        ExtornarPagoRQ extornarPagoRQ = jsonUtil.parseFromJSONFile(ExtornarPagoRQ.class, "ExtornarPagoRQ.json");
        ExtornarPagoRS extornarPagoRS = debtExtortService.extort(extornarPagoRQ);

    }

}