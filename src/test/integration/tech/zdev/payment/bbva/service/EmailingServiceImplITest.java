package tech.zdev.payment.bbva.service;

import junit.framework.TestCase;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import tech.zdev.khipu.commons.receipt.bean.PaymentTransaction;
import tech.zdev.payment.bbva.bean.emailing.MailContent;
import tech.zdev.payment.bbva.util.JsonUtil;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("dev")
@AutoConfigureMockMvc
public class EmailingServiceImplITest extends TestCase {

    @Value("${mailing.to}")
    private String[] to;

    @Value("${mailing.cc}")
    private String[] cc;


    @Autowired
    private EmailingService emailingService;

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void shouldBeMessageOfRefundPayment() throws Exception {
        String template = "refund-payment";
        MailContent mailContent = new MailContent();
        mailContent.setSubject("Costamar Payments | Recaudo interconectado BBVA | Solicitud de Extorno de Pago");
        mailContent.setTemplate(template);
        PaymentTransaction paymentTransaction = loadPaymentTransaction();
        mailContent.setPaymentTransaction(paymentTransaction);

        String[] to = new String[1];
        to[0] = "ederrafo@gmail.com";
        mailContent.setTo(to);
        mailContent.setCc(cc);
        emailingService.sendMessage(mailContent);

    }

    private PaymentTransaction loadPaymentTransaction() {
        JsonUtil jsonUtil = new JsonUtil();
        PaymentTransaction paymentTransaction = jsonUtil.parseFromJSONFile(PaymentTransaction.class,
                "paymentTransactionRecaudationPaymentDebt.json");
        return paymentTransaction;
    }

}